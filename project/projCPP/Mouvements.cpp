#include <iostream>
#include <fstream>
#include <iomanip>
#include "Mouvements.h"
#include "cOperationCode.h"
#include "cMouvementOperations.h"
#include "cDefinitions.h"
#include "cUtils.h"

using namespace std;

Mouvements::Mouvements()
{

}

Mouvements::~Mouvements()
{

}

string Mouvements::ShowAccounts()
{
    ifstream f("operations.txt");
    if (!f)
    {
        return "operations.txt doesnt exist!";
    }

    map<int, cMouvementOperations> accounts;
    while (!f.eof())
    {
        int id = 0;
        long date = 0;
        int code = 0;
        double value = 0;
        char c;

        f >> id;
        f >> c;
        f >> date;
        f >> c;
        f >> code;
        f >> c;
        f >> value;

        if (id != 0)
        {
            int sign = cOperationCode::GetValueOperation(code);
            if (sign != 0)
            {
                accounts[id].Alter(value, code, sign);
            }
            else
            {
                LogAnomalies(id, code, date, value);
            }
        }
    }
    f.close();
    stringstream ss;
    for (auto a = accounts.begin(); a != accounts.end(); ++a)
    {
        ss << endl << "----------------------------------" << endl;
        ss << "Compte numero: " << a->first << endl << endl;
        ss << "\tTotal Retrait de " << a->second.GetTotalRetrait() << " Euros" << endl;
        ss << "\tTotal Depot de " << a->second.GetTotalDepot() << " Euros" << endl;
        ss << "\tTotal Carte Bleue de " << a->second.GetTotalCB() << " Euros" << endl;
        ss << "Total: " << a->second.GetTotalValue() << " Euros" << endl;
    }
    return ss.str();
}

void Mouvements::LogAnomalies(int id, int code, long date, double value)
{
    stringstream ss;
                ss << "Bad code: " << code << " at (" << id << " " << to_string(date)
                   << " "  << value << ")" << endl;
    ofstream f(ANOMALIES_FILE, ofstream::out | ofstream::app);
    time_t t = time(0);
    f << "At " << cUtils::TimeToString(&t) << " : " << ss.str();
    f.close();
}

