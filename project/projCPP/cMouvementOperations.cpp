#include "cMouvementOperations.h"
#include "cOperationCode.h"
#include "cDefinitions.h"

cMouvementOperations::cMouvementOperations(): m_totalRetrait(0), m_totalDepot(0), m_totalCB(0)
{

}

cMouvementOperations::~cMouvementOperations()
{

}

//Add and modify a new bank movement
void cMouvementOperations::Alter(double value, int code, int sign)
{
    auto op = cOperationCode::GetValueOperationToString(code);

    if (op == OP_CODE_DAB)
    {
        m_totalRetrait += value;
    }
    else if (op == OP_CODE_CB)
    {
        m_totalCB += value;
    }
    else if (op == OP_CODE_DEPOT)
    {
        m_totalDepot += value;
    }
    m_totalValue += sign * value;
}
