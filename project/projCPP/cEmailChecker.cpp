#include "cEmailChecker.h"

bool cEmailChecker::IsEmailValid(string email)
{
    //check if the first char is a letter
    //check for @ and . .. @ only 1 in string
    //check if . is after @
    //must be text after @
    int len = email.length();

    if (email.empty() || len <= 5)
    {
        return false;
    }
    if (!IsChar(email[0]))
    {
        return false;
    }

    int _at = -1;
    int _dot = -1;

    for (int i(0); i < len; i++)
    {
        if (email[i] == '@')
        {
            if (_at != -1)//second encounter of @
            {
                return false;
            }
            _at = i;
        }
        if (email[i] == '.')
        {
            if (_dot + 1 == i)//. after .
            {
                return false;
            }
            _dot = i;
        }
    }

    // if . is final char = not ok
    if (_dot + 1 == len)
    {
        return false;
    }

    //i didnt found any @ or .
    if (_at == -1 && _dot == -1)
    {
        return false;
    }

    //. is not after @
    if (_at > _dot)
    {
        return false;
    }

    if (_at + 1 == _dot)
    {
        return false;
    }

    return true;
}

bool cEmailChecker::IsChar(char c)
{
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}
