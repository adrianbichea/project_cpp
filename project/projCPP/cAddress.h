#ifndef CADDRESS_H_INCLUDED
#define CADDRESS_H_INCLUDED

#include <iostream>
#include <sstream>
#include "cMyExceptions.h"

using namespace std;

class cAddress
{
    public:
        cAddress();
        cAddress(string, string, string, string);
        ~cAddress();

        //get/set
        const string GetStreetName() { return m_streetName; }
        void SetStreetName(string value) { m_streetName = value; }

        const string GetStreetPlus() { return m_streetPlus; }
        void SetStreetPlus(string value) { m_streetPlus = value; }

        const string GetCP() { return m_CP; }
        void SetCP(string value) { m_CP = value; }

        const string GetCity();
        void SetCity(string value) { m_city = value;}

        void operator= (cAddress const&);

        void Show();
        string ConcatenateAddress();

    private:
        string m_streetName;
        string m_streetPlus;
        string m_CP;
        string m_city;
};

#endif // CADDRESS_H_INCLUDED
