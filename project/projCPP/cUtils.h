#ifndef CUTILS_H_INCLUDED
#define CUTILS_H_INCLUDED

#include <iostream>
using namespace std;

class cUtils
{
    public:
        static string ReplaceString(string, string, string);
        static void InitializeOperationCodes();
        static string TimeToString(time_t*);
        static string ToUppercase(string, bool=false);
        static string IntToString(int, int);
};

#endif // CUTILS_H_INCLUDED
