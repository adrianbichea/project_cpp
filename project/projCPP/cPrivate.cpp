#include "cPrivate.h"

cPrivate::cPrivate(int id, string name, cAddress address, string email, string surname, int day, int month, int year, eSex sex)
         :cPerson(id, name, address, email)
{
    SetSurname(surname);
    SetBirthday(day, month, year);
    SetSex(sex);
}

cPrivate::~cPrivate()
{
    //cout << "~cPrivate(" << ConcatenatePerson() << ")" << endl;
}

string cPrivate::GetSurname()
{
    return cUtils::ToUppercase(m_surname, true);
}

void cPrivate::SetSurname(string value)
{
    if (value.length() > MAX_STRING_LENGTH)
    {
        stringstream ss;
        ss << value.length() << " on string " << value;
        throw cMyExceptions(eMyException::EX_MAX_STRING_LENGTH_REACHED, ss.str());
    }
    m_surname = value;
}

void cPrivate::Show()
{
    string s = cPerson::Info("Particulier: ");
    cout << ConcatenatePerson(s) << endl;
}

string cPrivate::GetSex()
{
    return m_sex == eSex::FEMALE ? "Female" : "Male";
}


string cPrivate::ConcatenatePerson(string s)
{
    auto srpl = cUtils::ReplaceString(s, "{$surname}", GetSurname());
    stringstream ss;
    ss << endl << srpl << endl;
    ss << "\t" << GetSex() << endl;
    ss << "\t" << GetBirthday().ToString();
    return ss.str();
}
