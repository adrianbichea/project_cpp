#ifndef CMOUVEMENTOPERATIONS_H_INCLUDED
#define CMOUVEMENTOPERATIONS_H_INCLUDED

class cMouvementOperations
{
    public:
        cMouvementOperations();
        ~cMouvementOperations();

        double GetTotalRetrait() { return m_totalRetrait; }
        void SetTotalRetrait(double val) { m_totalRetrait = val; }
        double GetTotalDepot() { return m_totalDepot; }
        void SetTotalDepot(double val) { m_totalDepot = val; }
        double GetTotalCB() { return m_totalCB; }
        void SetTotalCB(double val) { m_totalCB = val; }
        double GetTotalValue() { return m_totalValue; }

        void Alter(double value, int code, int sign);

    private:
        double m_totalRetrait;
        double m_totalDepot;
        double m_totalCB;
        double m_totalValue;
};


#endif // CMOUVEMENTOPERATIONS_H_INCLUDED
