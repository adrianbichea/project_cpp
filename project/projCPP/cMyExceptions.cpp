#include "cMyExceptions.h"


cMyExceptions::cMyExceptions(int n, int val) throw()
{
    GetMessage(n, val != 0 ? to_string(val) : "");
}

cMyExceptions::cMyExceptions(int n, string val) throw()
{
    GetMessage(n, val);
}

cMyExceptions::~cMyExceptions() throw()
{

}

void cMyExceptions::GetMessage(int n, string val) const
{
    switch(n)
    {
        case eMyException::EX_INVALID_EMAIL:
            message = "'" + val + "' is an invalid email!";
            break;
        case eMyException::EX_MAX_STRING_LENGTH_REACHED:
            message = "Max string length of (" + val + ") reached!";
            break;
        case eMyException::EX_ID_ALREADY_EXIST:
            message = "Id";
            if (!val.empty())
            {
                message += " (" + val + ")";
            }
            message += " already exist!";
            break;
        case eMyException::EX_BAD_SIRET:
            message = "The siret size is (" + val + ")!";
            break;
        default:
            message = "Unknown error!";
            break;
    }
}

const char* cMyExceptions::what() const throw()
{
    return message.c_str();
}
