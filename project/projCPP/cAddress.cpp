
#include "cUtils.h"
#include "cAddress.h"

cAddress::cAddress() :m_streetName(""),  m_streetPlus(""), m_CP(""), m_city("")
{

}

//initialization
cAddress::cAddress(string streetName, string streetPlus, string CP, string city)
{
    SetStreetName(streetName);
    SetStreetPlus(streetPlus);
    SetCP(CP);
    SetCity(city);
}

//destruction
cAddress::~cAddress()
{
    //cout << "~cAddress(" << ConcatenateAddress() << ")" << endl;
}

const string cAddress::GetCity()
{
    return cUtils::ToUppercase(m_city);
}

void cAddress::operator=(cAddress const &adr)
{
    SetStreetName(adr.m_streetName);
    SetStreetPlus(adr.m_streetPlus);
    SetCP(adr.m_CP);
    SetCity(adr.m_city);
}

void cAddress::Show()
{
    cout << ConcatenateAddress() << endl;
}

string cAddress::ConcatenateAddress()
{
    std::stringstream ss;
    ss << GetStreetName() << ", " << GetStreetPlus() << endl << "\t" << GetCP() << ", " << GetCity();
    return ss.str();
}
