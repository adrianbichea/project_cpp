#ifndef CMOUVEMENTOPERATIONS_H
#define CMOUVEMENTOPERATIONS_H


class cMouvementOperations
{
    public:
        cMouvementOperations();
        ~cMouvementOperations();

        double Getm_totalRetrait() { return m_totalRetrait; }
        void Setm_totalRetrait(double val) { m_totalRetrait = val; }
        double Getm_totalDepot() { return m_totalDepot; }
        void Setm_totalDepot(double val) { m_totalDepot = val; }
        double Getm_totalCB() { return m_totalCB; }
        void Setm_totalCB(double val) { m_totalCB = val; }

    protected:

    private:
        double m_totalRetrait;
        double m_totalDepot;
        double m_totalCB;
};

#endif // CMOUVEMENTOPERATIONS_H
