#ifndef PERSON_H_INCLUDED
#define PERSON_H_INCLUDED

#include "cAddress.h"
#include "cDefinitions.h"

class cPerson
{
    public:
        cPerson();
        cPerson(int, string, cAddress, string);
        virtual ~cPerson();

        //get set
        int GetId() { return m_id; }
        void SetId(int value) { m_id = value; }

        string GetName();
        void SetName(string value);

        cAddress GetAddress() { return m_address; }
        void SetAddress(cAddress value) { m_address = value; }

        string GetEmail() { return m_email; }
        void SetEmail(string value);

        virtual void Show()=0;

        string Info(string);
        string ConcatenateData(string);

    private:
        int m_id;
        string m_name;
        cAddress m_address;
        string m_email;
};


#endif // PERSON_H_INCLUDED
