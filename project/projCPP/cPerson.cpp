#include "cPerson.h"
#include "cEmailChecker.h"
#include "cUtils.h"

cPerson::cPerson() : m_id(0), m_name(""), m_email("")
{
    //cout << "cPerson::cPerson()" << endl;
}

cPerson::cPerson(int id, string name, cAddress address, string email)
{
    if (!cEmailChecker::IsEmailValid(email))
    {
        throw cMyExceptions(eMyException::EX_INVALID_EMAIL, email);
    }
    SetId(id);
    SetName(name);
    SetAddress(address);
    SetEmail(email);
}

cPerson::~cPerson()
{
}

string cPerson::GetName()
{
    return cUtils::ToUppercase(m_name);
}

void cPerson::SetName(string value)
{
    if (value.length() > MAX_STRING_LENGTH)
    {
        stringstream ss;
        ss << value.length() << " on string " << value;
        throw cMyExceptions(eMyException::EX_MAX_STRING_LENGTH_REACHED, ss.str());
    }
    m_name = value;
}

void cPerson::SetEmail(string value)
{
    if (cEmailChecker::IsEmailValid(value))
    {
        m_email = value;
    }
}

string cPerson::Info(string s)
{
    return ConcatenateData(s);
}
string cPerson::ConcatenateData(string s)
{
    stringstream ss;
    ss << s;
    ss << cUtils::IntToString(GetId(), 5) << endl;
    ss << "\t" << GetName() << " {$surname}" << endl;
    ss << "\t" << m_address.ConcatenateAddress() << endl;
    ss << "\t" << GetEmail();
    return ss.str();
}

