#include "cPerson.h"
#include "cBigData.h"
#include "cAddress.h"
#include "cProfessional.h"
#include "cUtils.h"

#include "cOperationCode.h"
#include "Mouvements.h"

void execute();
void Initialize();

int main()
{
    Initialize();
    try
    {
        execute();

        Mouvements m;
        cout << m.ShowAccounts();
    }
    catch(exception &ex)
    {
        cout << "Error: " << ex.what() << endl;
    }

    return 0;
}

void execute()
{
    cBigData bd;
    bd.AddNew(new cPrivate(1,
                           "beTy",
                           cAddress("12, rue des Oliviers", "", "94000", "cReTEiL"),
                           "betty@gmail.com",
                           "dANIeL",
                           11, 12, 1985,
                           eSex::MALE)
              );
    bd.AddNew(new cPrivate(3,
                           "BODIN",
                           cAddress("10, rue des Oliviers", "Etage 2", "94300", "VINCENNES"),
                           "bodin@gmail.com",
                           "Justin",
                           5, 5, 1965,
                           eSex::MALE)
              );
    bd.AddNew(new cPrivate(5,
                           "BERRIS",
                           cAddress("15, rue des Republique", "", "94120", "FONTENAY SOUS BOIS"),
                           "berris@gmail.com",
                           "Karine",
                           6, 6, 1977,
                           eSex::FEMALE)
              );
    bd.AddNew(new cPrivate(7,
                           "ABENIR",
                           cAddress("25, rue de la Paix", "", "92100", "LA DEFENSE"),
                           "betty@gmail.com", "Alexandra",
                           4, 12, 1977,
                           eSex::FEMALE)
              );
    bd.AddNew(new cPrivate(9,
                           "BENSAID",
                           cAddress("3, avenue des Parcs", "", "93500", "ROISSY EN France"),
                           "bensaid@gmail.com",
                           "Georgia",
                           16, 4, 1976,
                           eSex::FEMALE)
              );
    bd.AddNew(new cPrivate(11,
                           "ABABOU",
                           cAddress("3, rue Lecourbe", "", "93200", "BAGNOLET"),
                           "ababou@gmail.com",
                           "Teddy",
                           10, 10, 1970,
                           eSex::MALE)
              );

    //PROFESSIONALS
    bd.AddNew(new cProfessional(2,
                                "AXA",
                                cAddress("125, rue LaFayette", "Digicode 1432", "94120", "FONTENAY SOUS BOIS"),
                                "info@axa.fr",
                                "12548795641122",
                                eProfessionalStatus::SARL,
                                cAddress("125, rue LaFayette", "Digicode 1432", "94120", "FONTENAY SOUS BOIS")
                                ));

    bd.AddNew(new cProfessional(4,
                                "PAUL",
                                cAddress("36, quai des Orfevres", "", "93500", "ROISSY EN France"),
                                "info@paul.fr",
                                "87459564455444",
                                eProfessionalStatus::EURL,
                                cAddress("10, esplanade de la Defense", "", "92060", "LA DEFENSE")
                                ));

    bd.AddNew(new cProfessional(6,
                                "PRIMARK",
                                cAddress("32, rue E. Renan", "Bat. C", "75002", "PARIS"),
                                "contact@primark.fr",
                                "08755897458455",
                                eProfessionalStatus::SARL,
                                cAddress("32, rue E. Renan", "Bat. C", "75002", "PARIS")
                                ));

    bd.AddNew(new cProfessional(8,
                                "ZARA",
                                cAddress("23, av P. Valery", "", "92100", "LA DEFENSE"),
                                "info@zara.fr",
                                "65895874587854",
                                eProfessionalStatus::SA,
                                cAddress("24, esplanade de la D�fense", "Tour Franklin", "92060", "LA DEFENSE")
                                ));

    bd.AddNew(new cProfessional(10,
                                "LEONIDAS",
                                cAddress("15, Place de la Bastille", "Fond de Cour", "75003", "PARIS"),
                                "contact@leonidas.fr",
                                "91235987456832",
                                eProfessionalStatus::SAS,
                                cAddress("10, rue de la Paix", "", "75008", "PARIS")
                                ));

    bd.Show();
}


void Initialize()
{
    cUtils::InitializeOperationCodes();
}
