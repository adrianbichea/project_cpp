#include "cOperationCode.h"

map<int, cOperation> cOperationCode::m_operation;

cOperation::cOperation(int value, string name)
{
    this->value = value;
    this->name = name;
}

void cOperationCode::AddOperation(int n, cOperation o)
{
    m_operation[n] = o;
}

int cOperationCode::GetOperation(string name)
{
    for (auto o = m_operation.begin(); o != m_operation.end(); ++o)
    {
        if (o->second.GetName() == name)
        {
            return o->first;
        }
    }
    return 0;
}

int cOperationCode::GetValueOperation(int n)
{
    return m_operation[n].GetValue();
}

string cOperationCode::GetValueOperationToString(int n)
{
    return m_operation[n].GetName();
}
