#ifndef COPERATIONCODE_H_INCLUDED
#define COPERATIONCODE_H_INCLUDED

#include <iostream>
#include <map>
using namespace std;

class cOperation
{
    public:
        cOperation(int=0, string="");
        ~cOperation() {}

        int GetValue() { return value; }
        void SetValue(int value)  { this->value = value; }

        string GetName() { return name; }
        void SetName(string value) { name = value; }

    private:
        int value;//sign -1 (out) +1 (in)
        string name;
};

class cOperationCode
{
    public:
        static void AddOperation(int, cOperation);
        static int GetOperation(string);
        static int GetValueOperation(int);
        static string GetValueOperationToString(int);

    private:
        static map<int, cOperation> m_operation;
};

#endif // COPERATIONCODE_H_INCLUDED
