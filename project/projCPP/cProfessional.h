#ifndef CPROFESSIONAL_H_INCLUDED
#define CPROFESSIONAL_H_INCLUDED

#define SIRET_SIZE 14

#include "cPerson.h"

enum eProfessionalStatus
{
    SARL = 0,
    SA,
    SAS,
    EURL
};

class cProfessional : public cPerson
{
    public:
        cProfessional(int, string, cAddress, string, string /*siret*/, eProfessionalStatus/*status*/, cAddress/*siege*/);
        ~cProfessional();

        string GetSiret() { return m_siret;}
        void SetSiret(string value);

        eProfessionalStatus GetStatus() { return m_status; }
        string GetStatusToString();
        void SetStatus(eProfessionalStatus value) { m_status = value; }//this need to be changed

        cAddress GetProAddress() { return m_proAddress; }
        void SetProAddress(cAddress value) { m_proAddress = value; }

        void Show() override;

    private:
        string m_siret;
        eProfessionalStatus m_status;
        cAddress m_proAddress;

        string ConcatenatePerson();
};

#endif // CPROFESSIONAL_H_INCLUDED
