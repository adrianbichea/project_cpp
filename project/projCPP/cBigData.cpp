#include "cMyExceptions.h"
#include "cBigData.h"

cBigData::~cBigData()
{
    for (auto it = begin(m_persons); it != end(m_persons); it++)
    {
        if (it->second != nullptr)
        {
            delete(it->second);
        }
    }
    m_persons.clear();
}

void cBigData::AddNew(cPerson* person)
{
    //check if the key exist, if not then add it to map
    if (m_persons.find(person->GetId()) == m_persons.end())
    {
        m_persons[person->GetId()] = person;
        return;
    }
    throw cMyExceptions(eMyException::EX_ID_ALREADY_EXIST, person->GetId());
}

void cBigData::Show()
{
    cout << "--------- SHOW ALL ----------" << endl;
    for (auto it = begin(m_persons); it != end(m_persons); it++)
    {
        it->second->Show();
    }
}

