#include <stdio.h>
#include <time.h>
#include <string>
#include <sstream>
#include <iostream>

#include "cUtils.h"
#include "cOperationCode.h"
#include "cDefinitions.h"

string cUtils::ReplaceString(string s, string srepl, string swith)
{
    string srpl = s;
    auto start = s.find(srepl, 0);
    if (start != string::npos)
    {
        srpl.replace(start, srepl.length(), swith);
    }
    return srpl;
}

void cUtils::InitializeOperationCodes()
{
    cOperationCode::AddOperation(1, cOperation(-1, OP_CODE_DAB));
    cOperationCode::AddOperation(2, cOperation(-1, OP_CODE_CB));
    cOperationCode::AddOperation(3, cOperation(+1, OP_CODE_DEPOT));
}

string cUtils::TimeToString(time_t *t)
{
    tm *ltm = localtime(t);
    std::stringstream date;
    date << ltm->tm_mday
         << "/"
         << 1 + ltm->tm_mon
         << "/"
         << 1900 + ltm->tm_year
         << " "
         << 1 + ltm->tm_hour
         << ":"
         << 1 + ltm->tm_min
         << ":"
         << 1 + ltm->tm_sec;
    return date.str();
}

string cUtils::ToUppercase(string str, bool onlyFirstLetter)
{
    string s = "";
    int idx = 0;
    for (auto c : str)
    {
        if (!onlyFirstLetter || idx++ == 0)
        {
            s += toupper(c);
        }
        else
        {
            s += tolower(c);
        }
    }
    return s;
}

string cUtils::IntToString(int n, int c)
{
    string s = to_string(n);
    int l = c - s.length();
    for (int i(0); i < l; i++)
    {
        s = "0" + s;
    }
    return s;
}

